<?php
/**
 * 基础控制器
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */
namespace app\home\controller;
use app\common\controller\Common;
use app\home\logic\FieldLogic;
class Base extends Common
{
    public function initialize()
    {
        parent::initialize();
        $this->fieldLogic = new FieldLogic();
        $this->pcfglobal = get_global();
        // 设置URL模式
        set_home_url_mode();
    }

}
