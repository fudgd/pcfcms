<?php
/**
 * 前端路由
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */
use think\facade\Route;
use think\facade\Db;
use think\facade\Cache;
$seo = tpCache('seo');
$seo_pseudo = $seo['seo_pseudo'];
$seo_rewrite_format = $seo['seo_rewrite_format'];
if ($seo_pseudo == 3){
    $lang_rewrite_str = '';
     // 精简伪静态
    if ($seo_rewrite_format == 1) {
        // 首页
        Route::get('$', 'Index/index');
        // 列表页
        Route::get('<tid>$', 'Lists/index');
        // 内容页
        Route::get('<dirname>/<aid>$', 'View/index');
        // 单页
        Route::get('single/<tid>$', 'Single/lists');
        // 会员
        //Route::get('user/login', 'user.Users/login');
        //Route::get('user/reg', 'user.Users/reg');
        //Route::get('user/logout', 'user.Users/logout');
        // 搜索
        Route::get('search$', 'Search/lists');
    }else{
        // 首页
        Route::get('$', 'Index/index');
        // 文章
        Route::get('article/<tid>$', 'Article/lists');
        Route::get('article/<dirname>/<aid>$', 'Article/view');
        // 会员
        Route::get('user/login', 'user.Users/login');
        Route::get('user/reg', 'user.Users/reg');
        Route::get('user/logout', 'user.Users/logout');
        // 单页
        Route::get('<tid>$', 'Single/lists');
        // 搜索
        Route::get('search$', 'Search/lists');
        // 自定义模型
        $cacheKey = "application_route_channeltype";
        $channeltype_row = Cache::get($cacheKey);
        if (empty($channeltype_row)) {
            $channeltype_row = Db::name('channel_type')->field('nid,ctl_name')->where('ifsystem', 0)->select()->toArray();
            Cache::tag('channeltype')->set($cacheKey, $channeltype_row, HOME_CACHE_TIME);
        }
        foreach ($channeltype_row as $value) {
            // 自定义模型列表
            Route::get($value['nid'].'/<tid>$', $value['ctl_name'].'/lists');
            // 自定义模型内容
            Route::get($value['nid'].'/<dirname>/<aid>$', $value['ctl_name'].'/view');
        }
    }
}
