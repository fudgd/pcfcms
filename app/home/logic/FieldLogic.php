<?php
/**
 * 字段逻辑
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */
namespace app\home\logic;

use think\facade\Db;
use think\facade\Request;
use think\facade\Cache;

class FieldLogic 
{
    /**
     * 查询解析模型数据用以页面展示
     * @param array  $data 表数据
     * @param intval $channel_id 模型ID
     * @param array  $batch 是否批量列表
     */
    public function getChannelFieldList($data, $channel_id = '', $batch = false)
    {
        if (!empty($data) && !empty($channel_id)) {
            // 获取模型对应的附加表字段信息
            $fieldInfo = Db::name('channelfield')->where('channel_id',$channel_id)->column('*', 'name');
            $data = $this->handleAddonFieldList($data, $fieldInfo, $batch);
        } else {
            $data = array();
        }
        return $data;
    }

    /**
     * 查询解析单个数据表的数据用以页面展示
     * @param array  $data 表数据
     * @param intval $channel_id 模型ID
     * @param array  $batch 是否批量列表
     */
    public function getTableFieldList($data, $channel_id = '', $batch = false)
    {
        if (!empty($data) && !empty($channel_id)) {
            // 获取自定义表字段信息
            $fieldInfo = Db::name('channelfield')->where('channel_id',$channel_id)->column('*', 'name');
            $data = $this->handleAddonFieldList($data, $fieldInfo, $batch);
        } else {
            $data = array();
        }
        return $data;
    }

    /**
     * 处理自定义字段的值
     * @param array $data 表数据
     * @param array $fieldInfo 自定义字段集合
     * @param array $batch 是否批量列表
     */
    public function handleAddonFieldList($data, $fieldInfo, $batch = false)
    {
        if (false !== $batch) {
            return $this->handleBatchAddonFieldList($data, $fieldInfo);
        }
        if (!empty($data) && !empty($fieldInfo)) {
            foreach ($data as $key => $val) {
                $dtype = !empty($fieldInfo[$key]) ? $fieldInfo[$key]['dtype'] : '';
                $dfvalue_unit = !empty($fieldInfo[$key]) ? $fieldInfo[$key]['dfvalue_unit'] : '';
                switch ($dtype) {
                    case 'int':
                    case 'float':
                    case 'decimal':
                    case 'text':
                    {
                        $data[$key.'_unit'] = $dfvalue_unit;
                        break;
                    }
                    case 'checkbox':
                    case 'imgs':
                    case 'files':
                    {
                        if (!is_array($val)) {
                            $val = !empty($val) ? explode(',', $val) : array();
                        }
                        // 支持子目录
                        foreach ($val as $k1 => $v1) {
                            $val[$k1] = handle_subdir_pic($v1);
                        }
                        break;
                    }
                    case 'htmltext':
                    {
                        $val = htmlspecialchars_decode($val);
                        // 追加指定内嵌样式到编辑器内容的img标签，兼容图片自动适应页面
                        $titleNew = !empty($data['title']) ? $data['title'] : '';
                        $val = img_style_wh($val, $titleNew);
                        // 支持子目录
                        $val = handle_subdir_pic($val, 'html');
                        break;
                    }
                    case 'decimal':
                    {
                        $val = number_format($val,'2','.',',');
                        break;
                    }
                    default:
                    {
                        // 支持子目录
                        if (is_string($val)) {
                            $val = handle_subdir_pic($val, 'html');
                            $val = handle_subdir_pic($val);
                        }
                        break;
                    }
                }
                $data[$key.'_unit'] = $dfvalue_unit;
                $data[$key] = $val;
            }
        }
        return $data;
    }

    /**
     * 列表批量处理自定义字段的值
     * @param array $data 表数据
     * @param array $fieldInfo 自定义字段集合
     */
    public function handleBatchAddonFieldList($data, $fieldInfo)
    {
        if (!empty($data) && !empty($fieldInfo)) {
            foreach ($data as $key => $subdata) {
                $data[$key] = $this->handleAddonFieldList($subdata, $fieldInfo);
            }
        }
        return $data;
    }

}
