<?php
// +----------------------------------------------------------------------
// | 模板设置
// +----------------------------------------------------------------------
$gzpcfglobal = get_global();
//第一步判断选择了那个模板
$theme = $gzpcfglobal['admin_config']['tpl_theme'];//默认模板
if(common_ismobile() == "wap"  && file_exists(ROOT_PATH."public/template/{$theme}/mobile")) {
    $themeTitle = "mobile/";
} else {
    $themeTitle = "pc/";
}
return [
    // 模板后缀
    'view_suffix'  => 'html',
    // 模板路径
    'view_path'    => 'template/' .$theme.'/'.$themeTitle,
    // 视图输出字符串内容替换
    'tpl_replace_string'       => [
        '{__JS_PATH}' =>  '/template/' .$theme.'/'.$themeTitle.'skin/js', 
        '{__IMG_PATH}' =>  '/template/' .$theme.'/'.$themeTitle.'skin/img', 
        '{__CSS_PATH}'  =>  '/template/' .$theme.'/'.$themeTitle.'skin/css', 
        '{__PUBLIC_PATH}'  =>  '/template/' .$theme.'/'.$themeTitle.'skin', 
        '{__IMAGE_PATH}' =>  '/template/' .$theme.'/'.$themeTitle.'skin/images',  
        '{__COMMON_PATH}' =>  '/common', 
        '{__ADMIN_PATH}' =>  '/admin',
    ],
];