<?php
/**
 * 全局配置文件
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */

$cacheKey = "global_channeltype";
$channeltype_row = \think\facade\Cache::get($cacheKey);
if (empty($channeltype_row)) {
    $channeltype_row = \think\facade\Db::name('channel_type')->field('id,nid,ctl_name')
        ->order('sort_order asc, id asc')
        ->select()->toArray();
    \think\facade\Cache::set($cacheKey, $channeltype_row, PCFCMS_CACHE_TIME);
}
$channeltype_list = [];
$allow_release_channel = [];
foreach ($channeltype_row as $key => $val) {
    $channeltype_list[$val['nid']] = $val['id'];
    if (!in_array($val['nid'], ['single','guestbook'])) {
        array_push($allow_release_channel, $val['id']);
    }
}

return array(
    // CMS根目录文件夹
    'wwwroot_dir' => ['app','backup','config','extend','public','route','runtime','vendor'],
    // 禁用栏目的目录名称
    'disable_dirname' => ['app','backup','config','extend','install','public','route','runtime','vendor','tags','search','user','users','member','reg','centre','login'],
    // 文档SEO描述截取长度，一个字符表示一个汉字或字母
    'arc_seo_description_length' => 125,
    // 栏目最多级别
    'arctype_max_level' => 3,
    // 导航最多级别
    'nav_max_level' => 3,
    // 模型标识
    'channeltype_list' => $channeltype_list,
    // 发布文档的模型ID
    'allow_release_channel' => $allow_release_channel,
    // 栏目自定义字段的channel_id值
    'arctype_channel_id' => -99,
    // 栏目表原始字段
    'arctype_table_fields' => array('id','channeltype','current_channel','parent_id','typename','dirname','dirpath','englist_name','grade','typelink','litpic','templist','tempview','seo_title','seo_keywords','seo_description','sort_order','is_hidden','is_part','admin_id','is_del','del_method','status','add_time','update_time'),
    'admin_config' => [
        'seo_pseudo'    => 1, // 默认纯动态URL模式，兼容不支持pathinfo环境
        'seo_dynamic_format'    => 1, // 1=兼容模式的URL，2=伪静态
        'web_mobile_domain_open'    => 1, // 默认开启手机独立域名
        'web_mobile_domain' => 'm', // 手机独立域名
        'response_type'  => 1, // 0 = 响应式模板，1 = 分离式模板
        'seo_html_arcdir' => tpCache('seo.seo_html_arcdir'),
        'seo_rewrite_format' => tpCache('seo.seo_rewrite_format'),
        'system_sql_mode' => tpCache('system.system_sql_mode'), // 数据库模式
        'seo_inlet' => tpCache('seo.seo_inlet'), // 0=保留入口文件，1=隐藏入口文件
        'tpl_theme' => tpCache('system.system_tpl_theme') ? tpCache('system.system_tpl_theme') : 'default', // 前台模板风格
    ],
    // URL中筛选标识变量
    'url_screen_var' => '_screen',

    //后台登录验证码配置
    'admin_login'   => [
        'is_on' => 1, //开关0关闭，1开启
    ],
    //前端登录验证码配置
    'home_users_login'   => [
        'is_on' => 0, //开关0关闭，1开启
    ],
    //前端注册验证码配置
    'home_users_reg'   => [
        'is_on' => 0, //开关0关闭，1开启
    ]
	
	
);