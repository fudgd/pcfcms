<?php
/**
 * 公共字段模型
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */

namespace app\common\model;

use think\Model;
use think\facade\Db;
use think\facade\Cache;

class Channelfield extends Model
{
    //初始化
    protected function initialize()
    {
        parent::initialize();
    }

    // 获取单条记录 小潘 by 2020.03.23
    public function getInfo($id, $field = '*')
    {
        $result = Db::name('Channelfield')->field($field)->where('id',$id)->find();
        return $result;
    }

    // 获取单条记录 小潘 by 2020.03.23
    public function getInfoByWhere($where, $field = '*')
    {
        $result = Db::name('Channelfield')->field($field)->where($where)->cache(true,PCFCMS_CACHE_TIME,"channelfield")->find();
        return $result;
    }

    // 默认模型字段 小潘 by 2020.03.23
    public function getListByWhere($map = array(), $field = '*', $index_key = '')
    {
        $result = Db::name('Channelfield')->field($field)
            ->where($map)
            ->order('sort_order asc, channel_id desc, id desc')
            ->select()
            ->toArray();
        if (!empty($index_key)) {
            $result = convert_arr_key($result, $index_key);
        }
        return $result;
    }
}