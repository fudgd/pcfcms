<?php
/***********************************************************
 * 栏目导航逻辑定义
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\logic;
use think\facade\Db;
use think\facade\Request;
use think\facade\Cache;

class NavLogic
{

    //检测是否有子菜单
    public function hasChildren($id)
    {
        if (is_array($id)) {
            $ids = array_unique($id);
            $row = Db::name('nav_list')
                    ->field('parent_id, count(nav_) AS total')
                    ->where('parent_id','IN',$ids)
                    ->group('parent_id')
                    ->column('parent_id');
            return $row;
        } else {
            $count = Db::name('nav_list')->where('parent_id', $id)->count('nav_id');
            return ($count > 0 ? 1 : 0);
        }
    }

    // 全部栏目
    public function GetAllArctype($type_id = 0)
    {
        $where = [];
        $where[] = ['current_channel','>=', 0];
        $where[] = ['is_del','=', 0];
        $where[] = ['status','=', 1];        
        $field = 'id, parent_id, typename, dirname, litpic';
        // 查询所有可投稿的栏目
        $ArcTypeData = Db::name('arctype')->field($field)->where($where)->select()->toArray();
        // 读取上级ID并去重读取上级栏目
        $ParentIds = array_unique(get_arr_column($ArcTypeData, 'parent_id'));
        if (!empty($ParentIds)){
            $ParentIds = implode(',',$ParentIds);
            $where1=[];
            $where1[]=['current_channel','>', 0];
            $where1[]=['is_del','=', 0];
            $where1[]=['status','=', 1];
            $where1[]=['id','IN', $ParentIds];
            $PidData = Db::name('arctype')->field($field)->where($where1)->whereOR('parent_id',0)->select()->toArray();
        }else{
            $where[] = ['parent_id','=',0];
            $PidData = Db::name('arctype')->field($field)->where($where)->select()->toArray();
        }
        // 合并顶级栏目
        static $seo_pseudo = null;
        if (null === $seo_pseudo) {
            $seoConfig = tpCache('seo');
            $pcfglobal = get_global();
            $seo_pseudo = !empty($seoConfig['seo_pseudo']) ? $seoConfig['seo_pseudo'] : $pcfglobal['admin_config.seo_pseudo'];
        }
        // 下拉框拼装
        $HtmlCode = '<select name="type_id" id="type_id" lay-filter="type_id">';
        $HtmlCode .= '<option id="arctype_default" value="0">请选择栏目</option>';
        static $domain = null;
        null === $domain && $domain = request::domain();
        foreach ($PidData as $yik => $yiv) {
            // 栏目路径
            if (2 == $seo_pseudo) {
                // 生成静态
                $typeurl = $domain."/index.php/lists/index.html?tid={$yiv['id']}&t=".time();
            } else {
                // 动态或伪静态
                $typeurl = typeurl("lists/index", $yiv, true, $domain, $seo_pseudo, tpCache('seo.seo_rewrite_format'));
            }
            $style1 = $type_id == $yiv['id'] ? 'selected' : '';
            if (0 == $yiv['parent_id']) {
                //一级下拉框
                $HtmlCode .= '<option value="'.$yiv['id'].'" data-typeurl="'.$typeurl.'" data-typename="'.$yiv['typename'].'" '.$style1.'>'.$yiv['typename'].'</option>';
                $type = 0;
            } else {
                //二级下拉框
                $HtmlCode .= '<option value="'.$yiv['id'].'" data-typeurl="'.$typeurl.'" data-typename="'.$yiv['typename'].'" '.$style1.'>&nbsp; &nbsp;'.$yiv['typename'].'</option>';
                $type = 1;
            }
            foreach ($ArcTypeData as $erk => $erv) {
                //栏目路径
                if (2 == $seo_pseudo) {
                    // 生成静态
                    $typeurl = $domain."/index.php/lists/index.html?tid={$erv['id']}&t=".time();
                } else {
                    // 动态或伪静态
                    $typeurl = typeurl("lists/index", $yiv, true, $domain, $seo_pseudo, tpCache('seo.seo_rewrite_format'));
                }
                if ($erv['parent_id'] == $yiv['id']) {
                    if (0 == $type) {
                        $style1 = $type_id == $erv['id'] ? 'selected' : '';
                        //二级下拉框
                        $HtmlCode .= '<option value="'.$erv['id'].'" data-typeurl="'.$typeurl.'" data-typename="'.$erv['typename'].'" '.$style1.'>&nbsp; &nbsp;'.$erv['typename'].'</option>';
                    } else {
                        //三级下拉框
                        $HtmlCode .= '<option value="'.$erv['id'].'" data-typeurl="'.$typeurl.'" data-typename="'.$erv['typename'].'">&nbsp; &nbsp; &nbsp; &nbsp;'.$erv['typename'].'</option>';
                    }
                }
            }
        }
        $HtmlCode .= '</select>';
        return $HtmlCode;
    }

    // 功能列表
    public function ForegroundFunction()
    {
        return [
            [
                'title' => '首页',
                'code'   => 'home_index',
                'url'   => '/',
            ],
            [
                'title' => '问答中心',
                'code'   => 'home_Bbs',
                'url'   => '/bbs/index.html',
            ],
            [
                'title' => '会员中心',
                'code'   => 'user_center',
                'url'   => '/user.users/users_center.html',
            ],
        ];
    }

    /**
     * 获取指定父级的子菜单
     * @param  integer $parent_id 父级ID
     * @return array
     */
    public function getNavigList($position_id, $parent_id = 0)
    {
        $row = Db::name('nav_list')
                ->where(['position_id'=>$position_id,'parent_id'=>$parent_id,'is_del'=>0])
                ->select()->toArray();
        return $row;
    }

    /**
     * 获得指定菜单下的子菜单的数组
     * @access  public
     * @param   int     $id     菜单的ID
     * @param   int     $selected   当前选中菜单的ID
     * @param   boolean $re_type    返回的类型: 值为真时返回下拉列表,否则返回数组
     * @param   int     $level      限定返回的级数。为0时返回所有级数
     * @param   array   $map      查询条件
     */
    public function nav_list($id = 0, $selected = 0, $re_type = true, $level = 0, $map = array(), $is_cache = true)
    {
        static $res = NULL;
        if ($res === NULL){
            $where = array('is_del' => 0);
            if (!empty($map)) {
                $where = array_merge($where, $map);
            }
            foreach ($where as $key => $val) {
                $key_tmp = 'c.'.$key;
                $where[$key_tmp] = $val;
                unset($where[$key]);
            }
            $fields = "c.*, count(s.nav_id) as has_children, '' as children";
            $res = DB::name('nav_list')
                ->field($fields)
                ->alias('c')
                ->join('nav_list s','s.parent_id = c.nav_id','LEFT')
                ->where($where)
                ->group('c.nav_id')
                ->order('c.parent_id asc, c.sort_order asc, c.nav_id')
                ->select()->toArray();
            $res = convert_arr_key($res,'nav_id');
        }
        if (empty($res) == true){
            $res = NULL;
            return $re_type ? '' : array();
        }
        $options = $this->navig_options($id, $res); // 获得指定菜单下的子菜单的数组
        if ($level > 0)
        {
            if ($id == 0){
                $end_level = $level;
            }else{
                $first_item = reset($options); // 获取第一个元素
                $end_level  = $first_item['level'] + $level;
            }
            foreach ($options AS $key => $val)
            {
                if ($val['level'] >= $end_level)
                {
                    unset($options[$key]);
                }
            }
        }
        $pre_key = 0;
        foreach ($options AS $key => $value)
        {
            $options[$key]['has_children'] = 0;
            if ($pre_key > 0)
            {
                if ($options[$pre_key]['nav_id'] == $options[$key]['parent_id'])
                {
                    $options[$pre_key]['has_children'] = 1;
                }
            }
            $pre_key = $key;
        }
        if ($re_type == true)
        {
            $select = '';
            foreach ($options AS $var)
            {
                $select .= '<option value="' . $var['nav_id'] . '" ';
                $select .= ($selected == $var['nav_id']) ? "selected='true'" : '';
                $select .= '>';
                if ($var['level'] > 0)
                {
                    $select .= str_repeat('&nbsp;', $var['level'] * 4);
                }
                $select .= htmlspecialchars(addslashes($var['nav_name'])) . '</option>';
            }
            $res = NULL;
            return $select;
        }else{
            $res = NULL;
            return $options;
        }
    }

    /**
     * 过滤和排序所有文章菜单，返回一个带有缩进级别的数组
     * @access  private
     * @param   int     $id     上级菜单ID
     * @param   array   $arr    含有所有菜单的数组
     * @param   int     $level  级别
     */
    public function navig_options($spec_id, $arr)
    {
        static $cat_options = array();
        if (isset($cat_options[$spec_id])){
            $cat_options = array();
            return $cat_options[$spec_id];
        }
        if (!isset($cat_options[0])){
            $level = $last_id = 0;
            $options = $id_array = $level_array = array();
            while (!empty($arr))
            {
                foreach ($arr AS $key => $value)
                {
                    $id = $value['nav_id'];
                    if ($level == 0 && $last_id == 0)
                    {
                        if ($value['parent_id'] > 0)
                        {
                            break;
                        }
                        $options[$id]          = $value;
                        $options[$id]['level'] = $level;
                        $options[$id]['nav_id']    = $id;
                        $options[$id]['nav_name']  = $value['nav_name'];
                        unset($arr[$key]);
                        if ($value['has_children'] == 0)
                        {
                            continue;
                        }
                        $last_id  = $id;
                        $id_array = array($id);
                        $level_array[$last_id] = ++$level;
                        continue;
                    }
                    if ($value['parent_id'] == $last_id)
                    {
                        $options[$id]          = $value;
                        $options[$id]['level'] = $level;
                        $options[$id]['nav_name']    = $id;
                        $options[$id]['nav_name']  = $value['nav_name'];
                        unset($arr[$key]);
                        if ($value['has_children'] > 0)
                        {
                            if (end($id_array) != $last_id)
                            {
                                $id_array[] = $last_id;
                            }
                            $last_id    = $id;
                            $id_array[] = $id;
                            $level_array[$last_id] = ++$level;
                        }
                    }
                    elseif ($value['parent_id'] > $last_id)
                    {
                        break;
                    }
                }
                $count = count($id_array);
                if ($count > 1){
                    $last_id = array_pop($id_array);
                }elseif ($count == 1){
                    if ($last_id != end($id_array)){
                        $last_id = end($id_array);
                    }else{
                        $level = 0;
                        $last_id = 0;
                        $id_array = array();
                        continue;
                    }
                }
                if ($last_id && isset($level_array[$last_id])){
                    $level = $level_array[$last_id];
                }else{
                    $level = 0;
                    break;
                }
            }
            $cat_options[0] = $options;
        }else{
            $options = $cat_options[0];
        }
        if (!$spec_id){
            $cat_options = array();
            return $options;
        }else{
            if (empty($options[$spec_id])){
                $cat_options = array();
                return array();
            }
            $spec_id_level = $options[$spec_id]['level'];
            foreach ($options AS $key => $value)
            {
                if ($key != $spec_id){
                    unset($options[$key]);
                }else{
                    break;
                }
            }
            $spec_id_array = array();
            foreach ($options AS $key => $value)
            {
                if (($spec_id_level == $value['level'] && $value['nav_id'] != $spec_id) || ($spec_id_level > $value['level'])){
                    break;
                }else{
                    $spec_id_array[$key] = $value;
                }
            }
            $cat_options[$spec_id] = $spec_id_array;
            $cat_options = array();
            return $spec_id_array;
        }
    }

}