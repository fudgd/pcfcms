<?php
/***********************************************************
 * 资源管理
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
use think\facade\Cache;
class Res extends Base
{
    public $popedom = '';
    public function initialize() {
        parent::initialize();
        $ctl_act = Request::controller().'/'.Request::action();
        $this->popedom = appfile_popedom($ctl_act);
    }
    public function index()
    {
        //验证查看权限
        if(!$this->popedom["list"]){
            return $this->errorNotice('您没有权限执行此操作',true,3,false);
        }
        return $this->fetch();
    }

}